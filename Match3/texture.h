#pragma once
#include "includes.h"

class Texture
{
protected:
	//Texture id
	unsigned int texture_id;
	std::string file;

public:
	Texture();
	virtual ~Texture();

	virtual bool LoadFromFile(std::string file_name);

	unsigned int TextureID() const;
	std::string TextureFilename() const;
};
