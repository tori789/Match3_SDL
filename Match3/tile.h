#pragma once
#include "includes.h"
#include "piece.h"

class Tile : public Updateable
{
	std::shared_ptr<Quad> square;
	std::shared_ptr<Piece> piece;
public:
	Tile(glm::vec3 position);

	~Tile();

	float X() const;
	float Y() const;
	float Z() const;
	bool Filled() const;

	void PlacePiece(std::shared_ptr<Piece> p);
	std::shared_ptr<Piece> PlacedPiece() const;

	void DestroyPiece();
	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration);
};