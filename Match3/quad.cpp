#include "quad.h"

Quad::Quad(glm::vec3 pos, glm::vec2 size, glm::vec2 offset, std::shared_ptr<Texture> texture, std::shared_ptr<Shader> shader) : Updateable()
{
	this->pos = pos;
	this->size = size;
	this->offset = offset;
	this->shader = shader;
	this->texture = texture;
}

Quad::~Quad()
{
	glDeleteBuffers(1, &vao);
}

bool Quad::OnStart()
{		
	//VBO data
	GLfloat vertex_data[] =
	{
		//     X				Y          Z     U     V
		- 0.5f * size.x + offset.x, - 0.5f * size.y + offset.y, 0.0f, 0.0f, 0.0f,
		  0.5f * size.x + offset.x, - 0.5f * size.y + offset.y, 0.0f, 1.0f, 0.0f,
		  0.5f * size.x + offset.x,   0.5f * size.y + offset.y, 0.0f, 1.0f, 1.0f,
		- 0.5f * size.x + offset.x,   0.5f * size.y + offset.y, 0.0f, 0.0f, 1.0f,
	};

	vertex_count = 4;

	shader_pos = glGetAttribLocation(shader->ShaderProgram(), "aPos");
	shader_texture = glGetAttribLocation(shader->ShaderProgram(), "aTexCoord");

	//Create VAO
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	//Position
	glBindBuffer(GL_ARRAY_BUFFER, vao);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(shader_pos, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(shader_pos);
	
	//Texture
	glVertexAttribPointer(shader_texture, 2, GL_FLOAT, GL_TRUE, 5 * sizeof(GLfloat), (const GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(shader_texture);

	glBindVertexArray(0);

	return true;
}

bool Quad::OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration)
{
	//Bind program
	glUseProgram(shader->ShaderProgram());

	//Activate texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->TextureID());
	shader->SetInt("texture0", 0);

	//Send projection, view and model matrix
	shader->SetMat4("projection", projection);
	shader->SetMat4("view", view);
	shader->SetMat4("model", translate(glm::mat4(), glm::vec3(pos.x, pos.y, pos.z)));

	//Set vertex data
	glBindVertexArray(vao);

	//Render
	glDrawArrays(GL_QUADS, 0, 4);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Unbind program
	glUseProgram(NULL);

	return true;
}

void Quad::UpdatePositon(glm::vec2 position)
{ 
	this->pos.x = position.x; this->pos.y = position.y;
}

float Quad::X() const
{ 
	return pos.x;
}

float Quad::Y() const
{ 
	return pos.y;
}

float Quad::Z() const
{ 
	return pos.z;
}