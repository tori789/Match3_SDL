#include "camera.h"

Camera::Camera()
{
	const float zoom = 20.0f;
	//projection = glm::perspective(glm::radians(ZOOM), (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.1f, 100.0f);

	//Orthographic projection calculated based of the abovce perspective projection values
	camera_distance = 25.0f;
	auto aspect_ratio = WINDOW_WIDTH / (float)WINDOW_HEIGHT;
	const float a = WINDOW_HEIGHT / (float)WINDOW_WIDTH;
	float halfY = zoom * sqrt(1.0f + a * a) / 2.0f * glm::pi<float>() / 180.0f;
	float top = camera_distance * tan(halfY);
	float right = camera_distance * tan(halfY * aspect_ratio);
	projection = glm::ortho(-right, right, -top, top, 0.1f, 100.0f);

	//VIEW MATRIX
	glm::vec3 position = glm::vec3(TILE_X * 0.5f, TILE_Y * 0.5f, camera_distance);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
	viewMatrix = glm::lookAt(position, position + front, up);
}

// This function returns world space coordinates on a plance situated with z = 0
glm::vec3 Camera::ScreenToZeroWorldPlaneCoords(int mouse_x, int mouse_y) {

	// NORMALISED DEVICE SPACE
	double x = 2.0 * mouse_x / WINDOW_WIDTH - 1;
	double y = -2.0 * mouse_y / WINDOW_HEIGHT + 1;

	// Projection/Eye Space
	glm::mat4 projection_view = projection * viewMatrix;
	glm::mat4 view_projection_inverse = inverse(projection_view);

	//calculate the ray
	glm::vec3 p0 = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec4 l0 = view_projection_inverse * glm::vec4(x, y, -1.0f, 1.0f);
	l0.w = 1.0f / l0.w;
	l0.x *= l0.w;
	l0.y *= l0.w;
	l0.z *= l0.w;

	glm::vec4 l1 = view_projection_inverse * glm::vec4(x, y, 1.0f, 1.0f);
	l1.w = 1.0f / l1.w;
	l1.x *= l1.w;
	l1.y *= l1.w;
	l1.z *= l1.w;

	glm::vec3 l = normalize(l1 - l0);
	glm::vec3 n = glm::vec3(0.0f, 0.0f, 1.0f);
	auto distance = dot((p0 - glm::vec3(l0)), n) / dot(l, n);

	glm::vec3 p = glm::vec3(l0) + l * distance;
	return p;
}