#include "piece_count.h"

PieceCount::PieceCount(int count, PieceType type)
{
	this->count = count;
	this->type = type;
}

PieceCount::~PieceCount()
{

}

void PieceCount::Increment()
{ 
	count++; 
}

void PieceCount::Decrement()
{ 
	count--; 
}

PieceType PieceCount::Type() const
{ return type; 
}

int PieceCount::Count() const
{
	return count; 
}