#pragma once
#include "includes.h"
#include "shader.h"

class Updateable
{
public:

	virtual ~Updateable() {}

	virtual bool OnStart() = 0;
	virtual bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration) = 0;


};