#include "text_texture.h"

TextTexture::TextTexture(std::string text) : Texture()
{
	this->text = text;
}


bool TextTexture::LoadFromFile(std::string fontfile) 
{
	//this opens a font style and sets a size
	TTF_Font* Sans = TTF_OpenFont(fontfile.c_str(), 24);

	SDL_Color White = { 255, 255, 255 };  

	// Create a SDL surface with the loaded font and the current text
	auto sdl_surface_image = TTF_RenderText_Solid(Sans, text.c_str(), White); 

	auto sucessfull = false;
	if (sdl_surface_image)
	{
		glBindTexture(GL_TEXTURE_2D, texture_id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sdl_surface_image->w, sdl_surface_image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, sdl_surface_image->pixels);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
		sucessfull = true;
	}
	else
	{
		std::cout << "Failed to load font" << std::endl;
		sucessfull = false;
	}
	SDL_FreeSurface(sdl_surface_image);

	return sucessfull;
}
