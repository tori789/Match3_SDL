#include "tile.h"

Tile::Tile(glm::vec3 position)
{
	auto v_shader = "vertex_shader.glsl";
	auto f_shader = "fragment_shader.glsl";
	square = std::make_shared<Quad>(
		position, glm::vec2(1, 1), glm::vec2(0, 0), 
		TextureManager::GetInstance()->CreateTexture("tile.png", "tile.png"),
		ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
}

Tile::~Tile()
{

}

bool Tile::OnStart()
{
	return square->OnStart();
}

bool Tile::OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration)
{
	return square->OnRender(projection, view, lastFrameDuration);
}

void Tile::DestroyPiece()
{
	piece.reset();
}

float Tile::X() const
{
	return square->X();
}

float Tile::Y() const
{
	return square->Y();
}
float Tile::Z() const
{
	return square->Z();
}

bool Tile::Filled() const
{
	return piece != NULL;
}

void Tile::PlacePiece(std::shared_ptr<Piece> p)
{ 
	piece = p; 
}

std::shared_ptr<Piece> Tile::PlacedPiece() const
{ 
	return piece; 
}