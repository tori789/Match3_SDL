#pragma once
#include "includes.h"
#include "piece_type.h"

class PieceCount
{
	int count;
	PieceType type;

public:
	PieceCount(int count, PieceType type);
	~PieceCount();

	void Increment();
	void Decrement();
	PieceType Type() const;
	int Count() const;
};