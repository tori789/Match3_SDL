#include "piece.h"

Piece::Piece(glm::vec3 position, PieceType type)
{
	auto v_shader = "vertex_shader.glsl";
	auto f_shader = "fragment_shader.glsl";
	this->type = type;
	switch (type)
	{
		case(RED):
			square = std::make_unique<Quad>(
				position, glm::vec2(1, 1), glm::vec2(0, 0), 
				TextureManager::GetInstance()->CreateTexture("tile_red.png", "tile_red.png"),
				ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
			break;
		case(GREEN):
			square = std::make_unique<Quad>(
				position, glm::vec2(1, 1), glm::vec2(0, 0), 
				TextureManager::GetInstance()->CreateTexture("tile_green.png", "tile_green.png"),
				ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
			break;
		case(BLUE):
			square = std::make_unique<Quad>(
				position, glm::vec2(1, 1), glm::vec2(0, 0), 
				TextureManager::GetInstance()->CreateTexture("tile_blue.png", "tile_blue.png"),
				ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
			break;
		case(PINK):
			square = std::make_unique<Quad>(
				position, glm::vec2(1, 1), glm::vec2(0, 0), 
				TextureManager::GetInstance()->CreateTexture("tile_pink.png", "tile_pink.png"),
				ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
			break;
	}
	goal_x = square->X();
	goal_y = square->Y();
}

Piece::~Piece()
{

}

void Piece::Move(glm::vec2 offset)
{
	this->goal_x = this->goal_x + offset.x;
	this->goal_y = this->goal_y + offset.y;
}

bool Piece::OnStart()
{
	return square->OnStart();
}

bool Piece::OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration)
{
	auto x = glm::mix((float)square->X(), (float)goal_x, lastFrameDuration * 15.0f);
	auto y = glm::mix((float)square->Y(), (float)goal_y, lastFrameDuration * 15.0f);
	square->UpdatePositon(glm::vec2(x, y));
	return square->OnRender(projection, view, lastFrameDuration);
}

PieceType Piece::Type() const
{
	return type;
}
