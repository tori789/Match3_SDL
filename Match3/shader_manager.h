#pragma once
#include "shader.h"

class ShaderManager
{

	//Current Shaders
	std::unique_ptr<std::unordered_map<std::string, std::shared_ptr<Shader>>> shaders;

	ShaderManager();

public:

	static std::shared_ptr<ShaderManager> GetInstance()
	{
		static std::shared_ptr<ShaderManager> sessionManager(new ShaderManager());
		return sessionManager;
	}
	//TODO make it private
	~ShaderManager();

	std::shared_ptr<Shader> CreateShader(std::string vertexShader, std::string fragmentShader);
	std::shared_ptr<Shader> Shader(std::string vertexShader, std::string fragmentShader);
};