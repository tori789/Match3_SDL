#pragma once
#include "text_texture.h"

class TextureManager
{

	//Current active textures
	std::unique_ptr<std::unordered_map<std::string, std::shared_ptr<Texture>>> textures;

	TextureManager();

public:

	static std::shared_ptr<TextureManager> GetInstance()
	{
		static std::shared_ptr<TextureManager> sessionManager(new TextureManager());
		return sessionManager;
	}
	//TODO make it private
	~TextureManager();

	std::shared_ptr<Texture> CreateTexture(std::string filename, std::string id);
	std::shared_ptr<Texture> CreateTextTexture(std::string font, std::string text, std::string id);
	std::shared_ptr<Texture> Texture(std::string filename);
};