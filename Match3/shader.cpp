#include "shader.h"

void Shader::PrintProgramLog(GLuint program)
{
	//Make sure name is shader
	if (glIsProgram(program))
	{
		//Program log length
		int info_log_length = 0;
		int max_length = info_log_length;

		//Get info string length
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &max_length);

		//Allocate string
		char* info_log = new char[max_length];

		//Get info log
		glGetProgramInfoLog(program, max_length, &info_log_length, info_log);
		if (info_log_length > 0)
		{
			//Print Log
			std::cout << info_log << std::endl;
		}

		//Deallocate string
		delete[] info_log;
	}
	else
	{
		std::cout << "Name " << program <<" is not a program" << std::endl;
	}
}

std::string Shader::ReadFromFile(const char* filename)
{
	std::ifstream ifs(filename);
	std::string content((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
	ifs.clear();
	ifs.close();
	return content;
}

Shader::Shader(std::string vertexShaderName, std::string fragmentShaderName)
{
	bool success = true;

	//Generate program
	g_program_id = glCreateProgram();

	//Create vertex shader
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	auto var = ReadFromFile(vertexShaderName.c_str());
	auto vertex_shader_source = var.c_str();

	//Set vertex source
	glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);

	//Compile vertex sourcet
	glCompileShader(vertex_shader);

	//Check vertex shader for errors
	GLint v_shader_Compiled = GL_FALSE;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &v_shader_Compiled);
	if (v_shader_Compiled != GL_TRUE)
	{

		std::cout << "Unable to compile vertex shader " << vertex_shader << std::endl;
		this->PrintProgramLog(vertex_shader);
		success = false;
	}
	else
	{
		//Attach vertex shader to program
		glAttachShader(g_program_id, vertex_shader);


		//Create fragment shader
		GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

		//Get fragment source
		std::string var = ReadFromFile(fragmentShaderName.c_str());
		auto fragment_shader_source = var.c_str();

		//Set fragment source
		glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);

		//Compile fragment source
		glCompileShader(fragment_shader);

		//Check fragment shader for errors
		GLint f_shader_compiled = GL_FALSE;
		glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &f_shader_compiled);
		if (f_shader_compiled != GL_TRUE)
		{
			std::cout << "Unable to compile fragment shader " << fragment_shader << std::endl;
			this->PrintProgramLog(fragment_shader);
			success = false;
		}
		else
		{
			//Attach fragment shader to program
			glAttachShader(g_program_id, fragment_shader);


			//Link program
			glLinkProgram(g_program_id);

			//Check for errors
			GLint program_success = GL_TRUE;
			glGetProgramiv(g_program_id, GL_LINK_STATUS, &program_success);
			if (program_success != GL_TRUE)
			{
				std::cout << "Error linking program " << g_program_id << std::endl;
				PrintProgramLog(g_program_id);
				success = false;
			}
		}
		glDeleteShader(fragment_shader);
	}
	glDeleteShader(vertex_shader);
}

Shader::~Shader()
{
	glDeleteProgram(g_program_id);
}

void Shader::SetBool(const std::string &name, bool value) const
{
	glUniform1i(glGetUniformLocation(g_program_id, name.c_str()), (int)value);
}

void Shader::SetInt(const std::string &name, int value) const
{
	glUniform1i(glGetUniformLocation(g_program_id, name.c_str()), value);
}

void Shader::SetFloat(const std::string &name, float value) const
{
	glUniform1f(glGetUniformLocation(g_program_id, name.c_str()), value);
}

void Shader::SetVec2(const std::string &name, const glm::vec2 &value) const
{
	glUniform2fv(glGetUniformLocation(g_program_id, name.c_str()), 1, &value[0]);
}

void Shader::SetVec2(const std::string &name, float x, float y) const
{
	glUniform2f(glGetUniformLocation(g_program_id, name.c_str()), x, y);
}

void Shader::SetVec3(const std::string &name, const glm::vec3 &value) const
{
	glUniform3fv(glGetUniformLocation(g_program_id, name.c_str()), 1, &value[0]);
}
void Shader::SetVec3(const std::string &name, float x, float y, float z) const
{
	glUniform3f(glGetUniformLocation(g_program_id, name.c_str()), x, y, z);
}

void Shader::SetVec4(const std::string &name, const glm::vec4 &value) const
{
	glUniform4fv(glGetUniformLocation(g_program_id, name.c_str()), 1, &value[0]);
}
void Shader::SetVec4(const std::string &name, float x, float y, float z, float w) const
{
	glUniform4f(glGetUniformLocation(g_program_id, name.c_str()), x, y, z, w);
}

void Shader::SetMat2(const std::string &name, const glm::mat2 &mat) const
{
	glUniformMatrix2fv(glGetUniformLocation(g_program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::SetMat3(const std::string &name, const glm::mat3 &mat) const
{
	glUniformMatrix3fv(glGetUniformLocation(g_program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::SetMat4(const std::string &name, const glm::mat4 &mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(g_program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}