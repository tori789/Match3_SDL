#include "session_manager.h"
#include "includes.h"

//Starts up SDL, creates window, and initializes OpenGL
bool init();

//The window we'll be rendering to
SDL_Window* g_window = 0;

//OpenGL context
SDL_GLContext g_context;

//Game session manager
std::shared_ptr<SessionManager> sessionManager = 0;

bool initGl()
{
	/* Configuration of the Open GL renderer */
	/*Enable back face culling*/
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	/*--------------------*/

	/*Blending*/
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	/*--------------------*/

	/* Depth buffer setup */
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	/*--------------------*/
	return true;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		g_window = SDL_CreateWindow("Match 3", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (g_window == NULL)
		{
			std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			//Create context
			g_context = SDL_GL_CreateContext(g_window);
			if (g_context == NULL)
			{
				std::cout << "OpenGL context could not be created! SDL Error: "  << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					std::cout << "Error initializing GLEW! " << glewGetErrorString(glewError) << std::endl;
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					std::cout << "Warning: Unable to set VSync! SDL Error: " << SDL_GetError() << std::endl;
				}

				//Configure opengl
				if(!initGl())
				{
					std::cout << "Error initializing OpenGl! " << std::endl;
				}

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
					success = false;
				}

				//Init Session
				sessionManager = SessionManager::GetInstance();
				if (!sessionManager->OnStart(g_window))
				{
					std::cout << "Unable to initialize GameManager! " << std::endl;
					success = false;
				}
			}
		}
	}
	return success;
}

void OnCleanup()
{
	/* Delete our opengl context, destroy our window, and shutdown SDL */
	SDL_GL_DeleteContext(g_context);

	//Deallocate shader programs
	sessionManager->Close();

	//Destroy window	
	SDL_DestroyWindow(g_window);

	//Quite image SDL
	IMG_Quit();

	//Quit SDL subsystems
	SDL_Quit();

	sessionManager.reset();
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		std::cout << "Failed to initialize!" << std::endl;
	}
	else
	{
		sessionManager->OnRender();
	}

	//Free resources and close SDL
	OnCleanup();

	return 0;
}