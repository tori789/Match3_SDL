#include "texture_manager.h"

TextureManager::TextureManager()
{
	textures = std::make_unique <std::unordered_map<std::string, std::shared_ptr<::Texture>>>();
}

TextureManager::~TextureManager()
{

}

std::shared_ptr<Texture> TextureManager::Texture(std::string filename)
{
	auto iter = textures->find(filename);
	if (iter == textures->end())
	{
		return nullptr;
	}
	else
	{
		return iter->second;
	}
}

std::shared_ptr<Texture> TextureManager::CreateTexture(std::string filename, std::string id)
{
	auto iter = Texture(id);
	if (iter != nullptr)
	{
		return iter;
	}
	else
	{
		auto t = std::make_shared<::Texture>();
		t->LoadFromFile(filename);
		textures->insert(std::make_pair(id, std::move(t)));
		return textures->at(id);
	}
}


std::shared_ptr<Texture> TextureManager::CreateTextTexture(std::string font, std::string text, std::string id)
{
	auto iter = Texture(id);
	if (iter != nullptr)
	{
		return iter;
	}
	else
	{
		std::shared_ptr<::Texture> t = std::make_shared<TextTexture>(text);
		t->LoadFromFile(font);
		textures->insert(std::make_pair(id, std::move(t)));
		return textures->at(id);
	}
}

