#pragma once
#include "texture.h"

class TextTexture : public Texture
{
	std::string text;

public:
	TextTexture(std::string text);

	bool LoadFromFile(std::string fontfile) override;
};