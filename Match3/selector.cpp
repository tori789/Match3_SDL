#include "selector.h"

Selector::Selector(glm::vec3 position)
{
	auto v_shader = "vertex_shader.glsl";
	auto f_shader = "fragment_shader.glsl";
	square = std::make_unique<Quad>(position, glm::vec2(2, 1), glm::vec2(0.5f, 0),
		TextureManager::GetInstance()->CreateTexture("selector.png", "selector.png"),
		ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
	goal_x = square->X();
	goal_y = square->Y();
}

Selector::~Selector()
{

}

void Selector::Move(glm::vec2 offset)
{
	this->goal_x = offset.x;
	this->goal_y = offset.y;
}


bool Selector::OnStart()
{
	return square->OnStart();
}

bool Selector::OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration)
{
	auto x = glm::mix((float)square->X(), (float)goal_x, lastFrameDuration * 15.0f);
	auto y = glm::mix((float)square->Y(), (float)goal_y, lastFrameDuration * 15.0f);
	square->UpdatePositon(glm::vec2(x, y));
	return square->OnRender(projection, view, lastFrameDuration);
}