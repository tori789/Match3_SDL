#include "shader_manager.h"

ShaderManager::ShaderManager()
{
	shaders = std::make_unique <std::unordered_map<std::string, std::shared_ptr<::Shader>>>();
}

ShaderManager::~ShaderManager()
{

}

std::shared_ptr<Shader> ShaderManager::Shader(std::string vertexShader, std::string fragmentShader)
{
	auto iter = shaders->find(vertexShader + fragmentShader);
	if (iter == shaders->end())
	{
		return nullptr;
	}
	else
	{
		return iter->second;
	}
}

std::shared_ptr<Shader> ShaderManager::CreateShader(std::string vertexShader, std::string fragmentShader)
{
	auto id = vertexShader + fragmentShader;
	auto iter = Shader(vertexShader, fragmentShader);
	if (iter != nullptr)
	{
		return iter;
	}
	else
	{
		auto t = std::make_shared<::Shader>(vertexShader, fragmentShader);
		shaders->insert(std::make_pair(id, std::move(t)));
		return shaders->at(id);
	}
}
