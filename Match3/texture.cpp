#include "texture.h"

Texture::Texture()
{
	// load and create a texture 
	// -------------------------

	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
}

bool Texture::LoadFromFile(std::string file_name)
{
	// load image, create texture and generate mipmaps
	auto sdl_surface_image = IMG_Load(file_name.c_str());
	auto sucessfull = false;
	if (sdl_surface_image)
	{
		glBindTexture(GL_TEXTURE_2D, texture_id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sdl_surface_image->w, sdl_surface_image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, sdl_surface_image->pixels);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
		sucessfull = true;
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
		sucessfull = false;
	}
	SDL_FreeSurface(sdl_surface_image);
	return sucessfull;
}

Texture::~Texture()
{

}

unsigned int Texture::TextureID() const
{
	return texture_id;
}

std::string Texture::TextureFilename() const
{
	return file;
}