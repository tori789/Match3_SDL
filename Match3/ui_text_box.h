#pragma once
#pragma once
#include "includes.h"
#include "quad.h"
#include "piece_type.h"
#include "texture_manager.h"
#include "shader_manager.h"

class UiTextBox : public Updateable
{
	std::unique_ptr<Quad> square;
	std::unique_ptr<Quad> text;
public:
	UiTextBox(glm::vec3 position, std::string text);
	~UiTextBox();

	bool InBounds();
	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration);
};