#pragma once
#pragma once
#include "includes.h"
#include "quad.h"
#include "piece_type.h"
#include "texture_manager.h"
#include "shader_manager.h"

class Selector : public Updateable
{
	float goal_x;
	float goal_y;

	std::unique_ptr<Quad> square;
public:
	Selector(glm::vec3 position);
	~Selector();

	void Move(glm::vec2 offset);
	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration);
};