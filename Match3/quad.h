#pragma once
#include "updatable.h"
#include "texture.h"

class Quad : public Updateable
{
	//Vertex vertex array object
	GLuint vao = 0;

	GLint shader_pos;
	GLint shader_texture;

	//Position
	glm::vec3 pos;
	glm::vec2 size;
	glm::vec2 offset;

	std::shared_ptr<Shader> shader;
	std::shared_ptr<Texture> texture;
	int vertex_count;
public:
	
	Quad(glm::vec3 position, glm::vec2 size, glm::vec2 offset, std::shared_ptr<Texture> texture, std::shared_ptr<Shader> shader);
	~Quad();

	void UpdatePositon(glm::vec2 position);
	float X() const;
	float Y() const;
	float Z() const;

	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration);
};