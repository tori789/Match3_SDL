#include "session_manager.h"

SessionManager::SessionManager()
{
	camera = std::make_shared<Camera>();
	entities = std::make_unique<std::vector<std::shared_ptr<Updateable>>>();
	map = std::make_shared<Map>(TILE_X, TILE_Y);
	selector = std::make_shared<Selector>(glm::vec3(0.0f, 0.0f, 0.2f));

	entities->push_back(map);
	entities->push_back(selector);
}

SessionManager::~SessionManager()
{

}

void SessionManager::HandleKeys(unsigned char key, int x, int y)
{
	auto world_pos = camera->ScreenToZeroWorldPlaneCoords(x, y);
	if (world_pos.x >= -0.5f && world_pos.x < TILE_X - 0.5f)
	{
		if (world_pos.y >= -0.5f && world_pos.y < TILE_Y - 0.5f)
		{
			auto x = (int)round(abs(world_pos.x));
			auto y = (int)round(abs(world_pos.y));

			auto index = x + TILE_X * y;

			if (x == 0)
			{
				map->Switch(index, index + 1);
				map->MatchThreeColumn(x);
				map->MatchThreeColumn(x+1);
				map->MatchThreeRow(y);
			}
			else
			{
				map->Switch(index, index - 1);
				map->MatchThreeColumn(x);
				map->MatchThreeColumn(x - 1);
				map->MatchThreeRow(y);
			}
			CompactAllColumns();
		}
	}
}

bool SessionManager::OnStart(SDL_Window* window)
{
	this->window = window;
	auto sucess = true;

	for (auto ent : *entities) 
	{
		ent->OnStart();
	}

	init = sucess;
	return sucess;
}

void SessionManager::OnEvent(int x, int y)
{
	//Event handler
	SDL_Event e;

	//Handle events on queue
	while (SDL_PollEvent(&e) != 0)
	{
		//User requests quit
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}
		//Handle keypress with current mouse position
		else if (e.type == SDL_EventType::SDL_MOUSEBUTTONDOWN)
		{
			HandleKeys(e.text.text[0], x, y);
		}
	}
}

void SessionManager::CompactAllColumns()
{
	auto cycle = true;
	while (cycle)
	{
		auto sucessfull = false;
		for (int x = 0; x < TILE_X; x++)
		{
			//We will compact the columns and try to match three as well
			sucessfull = sucessfull || map->CompactColumn(x);
		}

		if (!sucessfull)
		{
			cycle = false;
		}
	}
}

void SessionManager::MoveTileMapUp()
{
	map->MoveTileMapUp();
}

void SessionManager::UpdateSelector(int x, int y)
{
	auto worldPos = camera->ScreenToZeroWorldPlaneCoords(x, y);
	if (worldPos.x >= -0.5f && worldPos.x < TILE_X - 0.5f)
	{
		if (worldPos.y >= -0.5f && worldPos.y < TILE_Y - 0.5f)
		{
			auto worldX = round(abs(worldPos.x));
			auto worldY = round(abs(worldPos.y));
			auto offset = 0.0f;

			if (worldX != 0)
			{
				offset = -1.0f;
			}

			selector->Move(glm::vec2(worldX + offset, worldY));
		}
	}
}

void SessionManager::OnRender()
{
	clock_t start;
	int frame_count = 0;

	double last_frame_duration = 0;
	double current_duration = 0;
	double upDuration = 0;

	//While application is running
	while (!quit)
	{
		start = std::clock();

		if (upDuration > MOVE_UP_TIMER)
		{
			map->MoveTileMapUp();
			upDuration = 0;
		}

		//Get the mouse coordinates
		int x = 0, y = 0;
		SDL_GetMouseState(&x, &y);
		UpdateSelector(x, y);
		OnEvent(x, y);

		// Set background color as cornflower blue
		glClearColor(0.0f, 0.0f, 0.0f, 1.f);

		// Clear color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

		//Render entities like the map, ui and the selector box
		for (auto ent : *entities)
		{
			ent->OnRender(camera->ProjectionMatrix(), camera->ViewMatrix(), (float)last_frame_duration);
		}

		//Update screen
		SDL_GL_SwapWindow(window);

		last_frame_duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
		current_duration += last_frame_duration;
		upDuration += last_frame_duration;
		frame_count++;

		//If the last frame duration was less than 60 fps
		if (last_frame_duration < 1 / 60.0f)
		{
			SDL_Delay((int)((1000.0 / 60.0) - last_frame_duration * 1000.0));
		}

		if (frame_count == 60)
		{
			std::cout << "FPS: " << frame_count / current_duration << std::endl;
			frame_count = 0;
			current_duration = 0;
		}
	}
}

bool SessionManager::Close()
{
	entities->clear();
	return true;
}