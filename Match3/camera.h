#pragma once
#include "includes.h"
using namespace glm;

class Camera
{
	glm::mat4 projection;
	glm::mat4 viewMatrix;
	float camera_distance;

public:
	Camera();

	~Camera()
	{

	}
	glm::mat4 ProjectionMatrix() { return projection; }
	glm::mat4 ViewMatrix() { return viewMatrix; }
	glm::vec3 ScreenToZeroWorldPlaneCoords(int mouse_x, int mouse_y);
};