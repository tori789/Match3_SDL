#include "map.h"

Map::Map(int x_tiles, int y_tiles)
{
	this->x_tiles = x_tiles;
	this->y_tiles = y_tiles;
	this->tile_count = x_tiles * y_tiles;

	tiles = std::make_unique<std::vector<std::shared_ptr<Tile>>>();

	red_tiles = std::make_shared<PieceCount>(0, RED);
	blue_tiles = std::make_shared<PieceCount>(0, BLUE);
	green_tiles = std::make_shared<PieceCount>(0, GREEN);
	pink_tiles = std::make_shared<PieceCount>(0, PINK);
}

Map::~Map()
{

}

bool Map::OnStart()
{
	float x = 0.0f, y = 0.0f, z = 0.0f;
	auto sucessfull = false;
	for (; y < y_tiles; y++)
	{
		for (; x < x_tiles; x++)
		{
			auto tile = std::make_shared<Tile>(glm::vec3(x, y, z));
			sucessfull = tile->OnStart();
			tiles->push_back(std::move(tile));
		}
		x = 0;
	}

	//Fill first three rows
	FillRow(0);
	FillRow(1);
	FillRow(2);

	return sucessfull;
}

bool Map::OnRender(glm::mat4 projection, glm::mat4 view, float last_frame_duration)
{
	auto sucessfull = false;
	for (int i = 0; i < tile_count; i++)
	{
		auto tile = tiles->at(i);
		sucessfull = tile->OnRender(projection, view, last_frame_duration);
		if (tile->Filled())
		{
			tile->PlacedPiece()->OnRender(projection, view, last_frame_duration);
		}
	}
	return sucessfull;
}

std::shared_ptr<PieceCount> Min(std::shared_ptr<PieceCount> a, std::shared_ptr<PieceCount> b)
{
	if (a->Count() < b->Count())
		return a;
	return b;
}


std::shared_ptr<PieceCount> Map::SmalestPieceType()
{
	return Min(Min(Min(red_tiles, blue_tiles), green_tiles), pink_tiles);
}

void Map::DestroyPiece(int index)
{
	auto tile = tiles->at(index);

	switch (tile->PlacedPiece()->Type())
	{
	case (BLUE):
		blue_tiles->Decrement();
		break;
	case (RED):
		red_tiles->Decrement();
		break;
	case (GREEN):
		green_tiles->Decrement();
		break;
	case (PINK):
		pink_tiles->Decrement();
		break;
	}
	tile->DestroyPiece();
}

bool Map::MatchCheckCycle(std::shared_ptr<Tile> tile, std::shared_ptr<Piece>& previous_piece, int& num_equal_tiles, std::shared_ptr<std::vector<std::shared_ptr<Tile>>>& matched_tiles)
{
	auto sucesfull = false;
	if (tile->Filled())
	{
		if ((previous_piece != NULL && tile->PlacedPiece()->Type() == previous_piece->Type()) || previous_piece == NULL)
		{
			num_equal_tiles++;
			matched_tiles->push_back(tile);
		}
		else
		{
			num_equal_tiles = 1;
			matched_tiles->clear();
			matched_tiles->push_back(tile);
		}
		previous_piece = tile->PlacedPiece();
	}
	else
	{
		num_equal_tiles = 0;
		previous_piece = NULL;
		matched_tiles->clear();
	}

	if (num_equal_tiles >= 3)
	{
		for (unsigned int t = 0; t < matched_tiles->size(); t++)
		{
			matched_tiles->at(t)->DestroyPiece();
			sucesfull = true;
		}
		matched_tiles->clear();
	}
	return sucesfull;
}

bool Map::MatchThreeRow(int y)
{
	int num_equal_tiles = 0;
	std::shared_ptr<Piece> previous_piece = NULL;
	auto matched_tiles = std::make_shared<std::vector<std::shared_ptr<Tile>>>();
	auto sucessfull = false;
	//We will check y row
	for (int i = 0; i < TILE_X; i++)
	{
		auto tile = tiles->at(y * TILE_X + i);
		sucessfull = sucessfull || MatchCheckCycle(tile, previous_piece, num_equal_tiles, matched_tiles);
	}
	return sucessfull;
}

bool Map::MatchThreeColumn(int x)
{
	int num_equal_tiles = 0;
	std::shared_ptr<Piece> previous_piece = NULL;
	auto matched_tiles = std::make_shared<std::vector<std::shared_ptr<Tile>>>();
	auto sucessfull = false;
	//We will check x column
	for (int i = 0; i < TILE_Y; i++)
	{
		auto tile = tiles->at(i * TILE_X + x);
		sucessfull = sucessfull || MatchCheckCycle(tile, previous_piece, num_equal_tiles, matched_tiles);
	}
	return sucessfull;
}

std::shared_ptr<Tile> Map::FindNextTileUpwards(int x, int y)
{
	for (int i = y; i < TILE_Y; i++)
	{
		auto tile = tiles->at(i * TILE_X + x);
		if (tile->Filled())
		{
			return tile;
		}
	}
	return NULL;
}

bool Map::CompactColumn(int x)
{
	auto sucessfull = false;
	//Lets count the number of pieces
	for (int i = 0; i < TILE_Y; i++)
	{
		auto tile = tiles->at(i * TILE_X + x);
		if (!tile->Filled())
		{
			auto nextTile = FindNextTileUpwards(x, i);
			if (nextTile != NULL)
			{
				Switch((int)tile->X() + (int)tile->Y() * TILE_X, (int)nextTile->X() + (int)nextTile->Y() * TILE_X);
				sucessfull = sucessfull || MatchThreeRow((int)tile->Y());
				sucessfull = sucessfull || MatchThreeColumn((int)tile->X());
			}
			else
			{
				break;
			}
		}
	}
	return sucessfull;
}

void Map::Switch(int index, int newIndex)
{
	auto tile = tiles->at(index);
	auto next_tile = tiles->at(newIndex);
	auto piece = tile->PlacedPiece();
	auto next_piece = next_tile->PlacedPiece();
	if (tile->Filled())
	{
		piece->Move(glm::vec2(next_tile->X() - tile->X(), next_tile->Y() - tile->Y()));
	}

	if (next_tile->Filled())
	{
		next_piece->Move(glm::vec2(tile->X() - next_tile->X(), tile->Y() - next_tile->Y()));
	}

	tile->PlacePiece(next_piece);
	next_tile->PlacePiece(piece);
}

void Map::FillRow(int row)
{
	auto row_start_Index = row * TILE_X;
	auto piece_type = SmalestPieceType();

	for (int i = row_start_Index; i < row_start_Index + TILE_X; i++)
	{
		auto tile = tiles->at(i);
		auto piece = std::make_shared<Piece>(glm::vec3(tile->X(), tile->Y(), 0.1f), piece_type->Type());
		piece->OnStart();
		tile->PlacePiece(piece);
		piece_type->Increment();
		piece_type = SmalestPieceType();
	}
}

void Map::MoveTileMapUp()
{
	//Delete the top line
	auto y = TILE_Y - 1;
	for (int x = 0; x < TILE_X; x++)
	{
		auto tile = tiles->at(y * TILE_X + x);
		if (tile->Filled())
		{
			tile->DestroyPiece();
		}
	}

	//Move the rest up
	auto size = TILE_X * TILE_Y;
	for (; y > 0; y--)
	{
		for (int x = 0; x < TILE_X; x++)
		{
			auto currentIndex = y * TILE_X + x;
			auto nextIndex = (y-1) * TILE_X + x;
			Switch(currentIndex, nextIndex);
		}
	}

	//Fill up the zero line
	FillRow(0);
}