#pragma once
#include "includes.h"
#include "tile.h"
#include "piece_count.h"
class Map : public Updateable {

	int tile_count;
	int x_tiles;
	int y_tiles;

	std::unique_ptr<std::vector<std::shared_ptr<Tile>>> tiles;

	std::shared_ptr<PieceCount> red_tiles;
	std::shared_ptr<PieceCount> blue_tiles;
	std::shared_ptr<PieceCount> green_tiles;
	std::shared_ptr<PieceCount> pink_tiles;

public:

	Map(int x_tiles, int y_tiles);
	~Map();

	void FillRow(int row);
	void Switch(int index, int nextIndex);

	void DestroyPiece(int index);
	bool MatchThreeRow(int y);
	bool MatchThreeColumn(int x);
	bool CompactColumn(int x);
	void MoveTileMapUp();

	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float last_frame_duration);

private:
	std::shared_ptr<Tile> FindNextTileUpwards(int x, int y);
	std::shared_ptr<PieceCount> SmalestPieceType();
	bool MatchCheckCycle(std::shared_ptr<Tile> tile, std::shared_ptr<Piece>& previous_piece, int& num_equal_tiles, std::shared_ptr<std::vector<std::shared_ptr<Tile>>>& matched_tiles);

};

