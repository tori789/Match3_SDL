#pragma once
#include "map.h"
#include "updatable.h"
#include "camera.h"
#include "quad.h"
#include "selector.h"

class SessionManager
{
	//Life each non 
	int score;

	//Main loop flag
	bool quit;

	//Main init flag
	bool init;

	//Current SDL_Window
	SDL_Window* window;
	
	//A simple box that follows the mouse and snaps to the tile map,
	// serves as a way to know what and where you are clicking
	std::shared_ptr<Selector> selector;
	
	//Camera class contains projection and view matrix
	std::shared_ptr<Camera> camera;

	//Stores all entities that are initiated on OnStart and updated on OnRender
	std::unique_ptr<std::vector<std::shared_ptr<Updateable>>> entities;

	//Current active map
	std::shared_ptr<Map> map;

	SessionManager();

public:

	static std::shared_ptr<SessionManager> GetInstance()
	{
		static std::shared_ptr<SessionManager> sessionManager(new SessionManager());
		return sessionManager;
	}
	//TODO make it private
	~SessionManager();

	bool OnStart(SDL_Window* window);
	void OnRender();
	void OnEvent(int x, int y);
	bool Close();
private:
	void UpdateSelector(int x, int y);
	void HandleKeys(unsigned char key, int x, int y);
	void CompactAllColumns();
	void MoveTileMapUp();

};