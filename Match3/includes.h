#pragma once

#include <iostream> 
#include <fstream>
#include <vector>
#include <memory>  
#include <tuple>
#include <unordered_map>
#include <cstdio>
#include <ctime>
#include "SDL_ttf.h"

#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <SDL_image.h>
#include <gl\glu.h>
#include <gl\gl.h>

#include <gtc/matrix_transform.hpp>

#include <stdio.h>
#include <string>
#include <assert.h>  

//Screen dimension constants
const int WINDOW_WIDTH = 600;
const int WINDOW_HEIGHT = 1000;

const int TILE_X = 6;
const int TILE_Y = 10;

const float MOVE_UP_TIMER = 3.5f;