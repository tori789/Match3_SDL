#include "ui_text_box.h"

UiTextBox::UiTextBox(glm::vec3 position, std::string text)
{
	auto v_shader = "vertex_shader.glsl";
	auto f_shader = "fragment_shader.glsl";
	auto font = "fffforwa.ttf";
	this->square = std::make_unique<Quad>(position, glm::vec2(2, 1), glm::vec2(0.5f, 0),
		TextureManager::GetInstance()->CreateTexture("TextBox.png", "TextBox.png"),
		ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));

	this->text = std::make_unique<Quad>(position + glm::vec3(0.0f, 0.0f, 0.1f), glm::vec2(2, 1), glm::vec2(0.5f, 0),
		TextureManager::GetInstance()->CreateTextTexture(font, text, font + text),
		ShaderManager::GetInstance()->CreateShader(v_shader, f_shader));
}

UiTextBox::~UiTextBox()
{

}

bool UiTextBox::InBounds()
{
	bool sucessfull = false;

	//TODO 

	return sucessfull;
}

bool UiTextBox::OnStart()
{
	return square->OnStart();
}

bool UiTextBox::OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration)
{
	return square->OnRender(projection, view, lastFrameDuration);
}