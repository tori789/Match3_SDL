#pragma once
#include "includes.h"
#include "quad.h"
#include "piece_type.h"
#include "texture_manager.h"
#include "shader_manager.h"

class Piece : public Updateable
{
	float goal_x;
	float goal_y;

	PieceType type;
	std::unique_ptr<Quad> square;
public:
	Piece(glm::vec3 position, PieceType type);
	~Piece();

	PieceType Type() const;

	void Move(glm::vec2 offset);
	bool OnStart();
	bool OnRender(glm::mat4 projection, glm::mat4 view, float lastFrameDuration);
};